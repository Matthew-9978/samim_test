import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:samim_test/src/constants/light_theme.dart';
import 'package:samim_test/src/core/di/injector.dart';
import 'package:samim_test/src/core/error/bloc_exception_handler.dart';
import 'package:flutter_gen/gen_l10n/strings.dart';
import 'package:samim_test/src/routing/animated_page_route_builder.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Injector.inject();
  Bloc.observer = BlocExceptionHandler();
  runApp(
    const MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final botToastBuilder = BotToastInit();

    return MaterialApp(
      title: 'Samim',
      theme: lightTheme,
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      supportedLocales: const [
        Locale('en', 'US'),
        Locale('fa', 'IR'),
      ],
      locale: const Locale('fa', 'IR'),
      builder: (context, child) => ResponsiveWrapper.builder(
        GestureDetector(
          onPanDown: (detail) =>
              FocusManager.instance.primaryFocus?.unfocus(),
          child: botToastBuilder(context, child),
        ),
        defaultScale: true,
        maxWidth: 900,
        breakpoints: [
          const ResponsiveBreakpoint.autoScale(450, name: MOBILE),
          const ResponsiveBreakpoint.resize(500, name: MOBILE),
        ],
      ),
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      onGenerateRoute: (settings) => AnimatedPageRouteBuilder(settings),
    );
  }
}
