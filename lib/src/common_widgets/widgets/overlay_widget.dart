import 'package:flutter/material.dart';
import 'package:samim_test/src/utils/helper_functions.dart';

enum OverlayAlignment {
  topRight,
  topLeft,
  bottomRight,
  bottomLeft,
  bottomCenter,
  topCenter
}

class OverlayWidget extends StatefulWidget {
  final Widget child;
  final bool showOverlay;
  final Widget overlayWidget;
  final OverlayAlignment overlayAlignment;
  final double? preferredHeight, preferredWidth;

  const OverlayWidget({
    Key? key,
    required this.child,
    required this.showOverlay,
    required this.overlayWidget,
    required this.overlayAlignment,
    this.preferredHeight,
    this.preferredWidth,
  }) : super(key: key);

  @override
  State<OverlayWidget> createState() => OverlayWidgetState();
}

class OverlayWidgetState extends State<OverlayWidget> {
  bool showOverlay = false;
  OverlayEntry? overlayEntry;
  late GlobalKey overlayKey;
  late OverlayState overlayState;
  late Size beginSize, endSize;

  @override
  void initState() {
    overlayKey = GlobalKey();
    showOverlay = widget.showOverlay;
    postFrameCallback(() {
      RenderBox renderBox = (widget.child.key as GlobalKey)
          .currentContext
          ?.findRenderObject() as RenderBox;
      beginSize = Size(renderBox.size.width, renderBox.size.height);
      endSize = Size(
        widget.preferredWidth ?? MediaQuery.of(context).size.width,
        widget.preferredHeight ?? 200,
      );
    });
    super.initState();
  }

  @override
  void didUpdateWidget(covariant OverlayWidget oldWidget) {
    showOverlay = widget.showOverlay;
    if (widget.showOverlay &&
        overlayEntry == null &&
        oldWidget.showOverlay == false) {
      postFrameCallback(() {
        _showOverlay(context);
      });
    } else if (!widget.showOverlay && overlayEntry != null) {
      dismissOverlay();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  void _showOverlay(BuildContext context) async {
    if (overlayEntry != null) return;
    overlayState = Overlay.of(context)!;
    overlayEntry = OverlayEntry(
      maintainState: true,
      builder: (context) {
        return StatefulBuilder(
          key: overlayKey,
          builder: (_, setState) => TweenAnimationBuilder(
            tween: SizeTween(
              begin: showOverlay ? beginSize : endSize,
              end: showOverlay ? endSize : const Size(0, 0),
            ),
            builder: (BuildContext context, Size? value, Widget? child) {
              return Positioned(
                top: verticalPosition,
                right: rightPosition,
                left: leftPosition,
                height: value!.height,
                child: child!,
              );
            },
            curve: Curves.ease,
            duration: const Duration(milliseconds: 300),
            child: Material(
              color: Colors.transparent,
              child: widget.overlayWidget,
            ),
          ),
        );
      },
    );
    overlayState.insert(overlayEntry!);
  }

  Future<void> dismissOverlay() async {
    if (overlayEntry == null) return;
    if (overlayEntry?.mounted == false) return;
    showOverlay = false;
    postFrameCallback(() {
      overlayKey.currentState?.setState(() {});
    });
    overlayEntry?.remove();
    overlayEntry = null;
    postFrameCallback(() {
      overlayKey.currentState?.setState(() {});
    });
  }

  double get verticalPosition {
    RenderBox? renderBox = (widget.child.key as GlobalKey)
        .currentContext
        ?.findRenderObject() as RenderBox?;

    if (widget.overlayAlignment == OverlayAlignment.bottomRight ||
        widget.overlayAlignment == OverlayAlignment.bottomLeft ||
        widget.overlayAlignment == OverlayAlignment.bottomCenter) {
      return (renderBox?.localToGlobal(Offset.zero).dy ?? 0) +
          renderBox!.size.height;
    } else {
      return (renderBox?.localToGlobal(Offset.zero).dy ?? 0);
    }
  }

  double? get rightPosition {
    if (widget.overlayAlignment == OverlayAlignment.bottomRight ||
        widget.overlayAlignment == OverlayAlignment.topRight ||
        widget.overlayAlignment == OverlayAlignment.bottomCenter ||
        widget.overlayAlignment == OverlayAlignment.topCenter) {
      return 16;
    } else {
      return null;
    }
  }

  double? get leftPosition {
    if (widget.overlayAlignment == OverlayAlignment.bottomRight ||
        widget.overlayAlignment == OverlayAlignment.topRight) {
      return null;
    } else {
      return 16;
    }
  }

  @override
  void dispose() {
    dismissOverlay();
    super.dispose();
  }
}
