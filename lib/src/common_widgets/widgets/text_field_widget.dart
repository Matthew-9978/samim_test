import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:samim_test/src/common_widgets/widgets/text_widget.dart';
import 'package:samim_test/src/utils/extesnsions/context_extensions.dart';

class CustomTextField extends StatefulWidget {
  final String? errorText;
  final String? label;
  final String? hintText;
  final TextInputType? textInputType;
  final bool isObscure;
  final EdgeInsets? padding;
  final ValueChanged? onChanged;
  final VoidCallback? onTap;
  final TextEditingController? controller;
  final bool readOnly;
  final List<TextInputFormatter>? inputFormatters;
  final String? initValue;
  final TextStyle? textStyle;
  final TextStyle? hintStyle;
  final Color? backgroundColor;
  final int? maxLines;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final bool? autofocus;
  final int? maxLength;
  final FocusNode? focusNode;
  final TextStyle? labelStyle;
  final double borderRadius;
  final Color? borderColor;
  final BorderRadius? customBorderRadius;
  final bool hasClearButton;

  const CustomTextField({
    Key? key,
    this.focusNode,
    this.textInputType,
    this.errorText,
    this.isObscure = false,
    this.autofocus,
    this.onChanged,
    this.onTap,
    this.prefixIcon,
    this.initValue,
    this.maxLines = 1,
    this.hintStyle,
    this.suffixIcon,
    this.backgroundColor,
    this.textStyle,
    this.padding,
    this.readOnly = false,
    this.controller,
    this.hintText,
    this.inputFormatters,
    this.maxLength,
    this.label,
    this.labelStyle,
    this.borderRadius = 16,
    this.borderColor,
    this.customBorderRadius,
    this.hasClearButton = true,
  }) : super(key: key);

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  late TextEditingController controller;

  @override
  void initState() {
    controller = widget.controller ?? TextEditingController();
    if (widget.initValue != null) {
      (controller
        ..text = widget.initValue!
        ..selection =
            TextSelection.collapsed(offset: widget.initValue!.length));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormField(
          autofocus: widget.autofocus ?? false,
          readOnly: widget.readOnly,
          focusNode: widget.focusNode,
          controller: controller,
          style: widget.textStyle ??
              context.textTheme.bodyMedium!.copyWith(
                color: Theme.of(context).colorScheme.tertiary,
                fontWeight: FontWeight.w400,
              ),
          maxLines: widget.maxLines,
          maxLength: widget.maxLength,
          keyboardType: widget.maxLines == null
              ? TextInputType.multiline
              : widget.textInputType,
          obscureText: widget.isObscure,
          scrollPadding: EdgeInsets.zero,
          textAlignVertical: TextAlignVertical.center,
          inputFormatters: widget.inputFormatters,
          cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
          decoration: InputDecoration(
            contentPadding: widget.padding ??
                const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            fillColor: widget.backgroundColor ??
                (widget.readOnly
                    ? Theme.of(context).colorScheme.background
                    : Theme.of(context).colorScheme.onBackground),
            filled: true,
            hoverColor: Colors.white,
            counterText: "",
            suffixIcon: widget.suffixIcon,
            prefixIcon: widget.prefixIcon,
            enabledBorder: _getEnabledBorder(context),
            focusedBorder: _getFocusedBorder(context),
            border: _getEnabledBorder(context),
            hintStyle: context.textTheme.bodyMedium!.copyWith(fontSize: 16),
            labelStyle: context.textTheme.bodyMedium!.copyWith(fontSize: 16),
            labelText: widget.label,
            hintText: widget.hintText,
          ),
          onChanged: (value) {
            widget.onChanged?.call(value);
            setState(() {});
          },
          onTap: () {
            widget.onTap?.call();
            if (controller.selection ==
                TextSelection.fromPosition(
                    TextPosition(offset: controller.text.length - 1))) {
              setState(() {
                controller.selection = TextSelection.fromPosition(
                    TextPosition(offset: controller.text.length));
              });
            }
          },
        ),
        AnimatedContainer(
          duration: const Duration(milliseconds: 100),
          height: widget.errorText != null ? 20:0,
          child: TextWidget.medium(
            widget.errorText ?? "",
            textAlign: TextAlign.start,
            context: context,
            additionalStyle:  TextStyle(fontSize: 12,color: context.colorScheme.error),
          ),
        ),
      ],
    );
  }

  InputBorder _getErrorBorder(BuildContext context) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(widget.borderRadius),
      ),
      borderSide: BorderSide(
        color: Theme.of(context).colorScheme.error,
      ),
    );
  }

  InputBorder _getEnabledBorder(BuildContext context) {
    if (widget.errorText != null) {
      return _getErrorBorder(context);
    }
    return OutlineInputBorder(
      borderRadius: widget.customBorderRadius ??
          BorderRadius.all(
            Radius.circular(widget.borderRadius),
          ),
      borderSide: BorderSide(
        color: widget.borderColor ?? Theme.of(context).colorScheme.outline,
      ),
    );
  }

  InputBorder _getFocusedBorder(BuildContext context) {
    if (widget.errorText != null) {
      return _getErrorBorder(context);
    }
    return OutlineInputBorder(
      borderRadius: widget.customBorderRadius ??
          BorderRadius.all(
            Radius.circular(widget.borderRadius),
          ),
      borderSide: BorderSide(
        color: widget.borderColor ??
            (widget.readOnly
                ? Theme.of(context).colorScheme.outline
                : Theme.of(context).colorScheme.onSurfaceVariant),
      ),
    );
  }
}
