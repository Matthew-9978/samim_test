import 'package:flutter/material.dart';

enum LoadingStateEnum { loading, loadNodData, loadComplete }

class LazyListViewWidget<ItemType> extends StatefulWidget {
  final List<ItemType> items;
  final Widget Function(ItemType item) itemBuilder;
  final Widget? separatorWidget;
  final List<ItemType> Function(int length) onLoadMore;

  const LazyListViewWidget({
    Key? key,
    required this.items,
    required this.itemBuilder,
    this.separatorWidget,
    required this.onLoadMore,
  }) : super(key: key);

  @override
  State<LazyListViewWidget> createState() =>
      _LazyListViewWidgetState<ItemType, LazyListViewWidget<ItemType>>();
}

class _LazyListViewWidgetState<ItemType, W extends LazyListViewWidget<ItemType>>
    extends State<W> {
  late ScrollController scrollController;
  LoadingStateEnum loadingState = LoadingStateEnum.loadComplete;
  List<ItemType> items = [];

  @override
  void initState() {
    super.initState();
    items = widget.items;
    scrollController = ScrollController();
    scrollListener();
  }

  @override
  void dispose() {
    scrollController.removeListener(() { });
    scrollController.dispose();
    super.dispose();
  }

  void scrollListener() {
    scrollController.addListener(() async {
      if (scrollController.position.maxScrollExtent - scrollController.offset <
          200) {
        fetchNewItems();
      }
    });
  }

  void fetchNewItems()  {
    if (loadingState == LoadingStateEnum.loadComplete) {
      loadingState = LoadingStateEnum.loading;
      final newItems = widget.onLoadMore(items.length);
      items.addAll(newItems);
      if (newItems.length < 20) {
        loadingState = LoadingStateEnum.loadNodData;
      } else {
        loadingState = LoadingStateEnum.loadComplete;
      }
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      controller: scrollController,
      itemBuilder: (context, index) => widget.itemBuilder(items[index]),
      itemCount: items.length,
      separatorBuilder: (context, index) =>
          widget.separatorWidget ?? const SizedBox.shrink(),
    );
  }
}
