import 'package:flutter/material.dart';
import 'package:samim_test/src/common_widgets/widgets/text_widget.dart';
import 'package:samim_test/src/utils/extesnsions/context_extensions.dart';

class CustomAppbar extends StatelessWidget implements PreferredSize {
  final String? title;
  final TextStyle? titleStyle;
  final bool hasBackButton;
  final bool centerTitle;
  final Widget? leading;
  final List<Widget>? actions;
  final VoidCallback? onPressBack;
  final Color? statusBarColor;
  final Color? backgroundColor;
  final double? toolbarHeight;

  const CustomAppbar({
    Key? key,
    this.title,
    this.titleStyle,
    this.leading,
    this.actions,
    this.hasBackButton = false,
    this.centerTitle = true,
    this.onPressBack,
    this.statusBarColor,
    this.backgroundColor,
    this.toolbarHeight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      systemOverlayStyle:
          Theme.of(context).appBarTheme.systemOverlayStyle!.copyWith(
                statusBarColor: statusBarColor,
              ),
      title: TextWidget.bold(
        title ?? "",
        context: context,
        additionalStyle: const TextStyle(fontSize: 18).merge(titleStyle),
      ),
      centerTitle: centerTitle,
      actions: actions,
      backgroundColor:
          backgroundColor ?? Theme.of(context).colorScheme.background,
      leading: hasBackButton
          ? IconButton(
              onPressed: onPressBack,
              icon: Icon(
                Icons.arrow_back,
                color: context.colorScheme.tertiary,
              ))
          : leading,
    );
  }

  @override
  Widget get child => const SizedBox();

  @override
  Size get preferredSize => Size.fromHeight(toolbarHeight ?? 60);
}
