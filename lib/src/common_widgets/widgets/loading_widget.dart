import 'package:flutter/material.dart';
import 'package:samim_test/src/utils/extesnsions/context_extensions.dart';

class Loading extends StatelessWidget {
  final bool? primaryLoading;
  final double? width;
  final double? height;

  const Loading({
    Key? key,
    this.primaryLoading = false,
    this.width,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height ?? 24,
      width: width ?? 24,
      child: CircularProgressIndicator(
        color: primaryLoading! ? context.colorScheme.primary : Colors.white,
      ),
    );
  }
}
