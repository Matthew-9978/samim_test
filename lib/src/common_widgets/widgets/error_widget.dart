import 'package:flutter/material.dart';

class CommonErrorWidget extends StatelessWidget {
  final String? error;
  const CommonErrorWidget({Key? key, this.error}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
