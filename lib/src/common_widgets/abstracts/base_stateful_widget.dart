import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:samim_test/src/common_widgets/widgets/app_bar_widget.dart';
import 'package:samim_test/src/core/bloc/base_cubit/base_cubit.dart';
import 'package:samim_test/src/utils/helper_functions.dart';

abstract class BaseStatefulWidget<W extends StatefulWidget, C extends Cubit>
    extends StatefulWidget {
  final bool includeHorizontalPadding;
  final bool includeVerticalPadding;
  final bool includeFab;
  final bool resizeToAvoidBottomSheet;

  const BaseStatefulWidget({
    Key? key,
    this.includeHorizontalPadding = true,
    this.includeVerticalPadding = true,
    this.resizeToAvoidBottomSheet = true,
    this.includeFab = false,
  }) : super(key: key);

  @override
  State<W> createState();
}

abstract class BaseStatefulWidgetState<T extends BaseStatefulWidget,
    C extends BaseCubit> extends State<T> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  C get cubit => context.read<C>();

  @override
  Widget build(BuildContext context) {
    onBuild(context);
    return WillPopScope(
      onWillPop: () async {
        return await onPop.call()?.call() ?? true;
      },
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: backgroundColor(context),
        drawer: drawer(),
        resizeToAvoidBottomInset: widget.resizeToAvoidBottomSheet,
        appBar: appBar(context),
        bottomNavigationBar: bottomWidget(),
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: widget.includeHorizontalPadding ? 16 : 0,
                vertical: widget.includeVerticalPadding ? 16 : 0),
            child: body(context),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    postFrameCallback(() {
      onFirstFrameBuilt();
      cubit.onViewCreated();
    });
    super.initState();
  }

  void onFirstFrameBuilt() => {};

  void onBuild(BuildContext context) => {};

  Color? backgroundColor(BuildContext context) => null;

  WillPopCallback? onPop() => null;

  Widget? drawer() => null;

  Widget? bottomWidget() => null;

  String? provideRouteName() => null;

  Widget body(BuildContext context);

  CustomAppbar? appBar(BuildContext context) => null;

  Function? onPageReload(BuildContext context) => null;

  void onFabClick(BuildContext context) => () {};
}
