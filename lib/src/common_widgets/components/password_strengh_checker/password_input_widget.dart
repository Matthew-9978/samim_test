import 'package:flutter/material.dart';
import 'package:samim_test/src/common_widgets/components/password_strengh_checker/password_helper_widget.dart';
import 'package:samim_test/src/common_widgets/widgets/overlay_widget.dart';
import 'package:samim_test/src/common_widgets/widgets/text_field_widget.dart';
import 'package:samim_test/src/utils/extesnsions/context_extensions.dart';
import 'package:samim_test/src/utils/helper_functions.dart';

class PasswordInputWidget extends StatefulWidget {
  final TextEditingController controller;
  final String? initValue;
  final ValueChanged onPasswordFieldChanged;
  final Function()? onTap;
  final bool? readOnly;
  final String? label;
  final bool? isObscure;
  final bool? showEyeIcon;
  final bool showStrengthChecker;
  final bool showPasswordHelper;
  final bool focusOnCreate;

  const PasswordInputWidget({
    Key? key,
    this.initValue,
    required this.onPasswordFieldChanged,
    required this.controller,
    this.onTap,
    this.readOnly,
    this.label,
    this.isObscure,
    this.showStrengthChecker = true,
    this.showEyeIcon,
    this.showPasswordHelper = false,
    this.focusOnCreate = true,
  }) : super(key: key);

  @override
  State<PasswordInputWidget> createState() => _PasswordInputWidgetState();
}

class _PasswordInputWidgetState extends State<PasswordInputWidget> {
  ValueNotifier<String> passwordNotifier = ValueNotifier("");
  GlobalKey passwordKey = GlobalKey();
  FocusNode focusNode = FocusNode();
  bool showOverlay = false;
  bool passwordVisible = false;

  @override
  void initState() {
    if (widget.focusOnCreate) {
      focusNode.requestFocus();
    }
    if (widget.showPasswordHelper) {
      focusNode.addListener(() {
        if (focusNode.hasFocus) {
          showOverlay = true;
        } else {
          showOverlay = false;
        }
        setState(() {});
      });
    }
    super.initState();
  }

  @override
  void dispose() {
    passwordNotifier.dispose();
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return OverlayWidget(
      showOverlay: widget.showPasswordHelper ? showOverlay : false,
      overlayAlignment: context.isRTL
          ? OverlayAlignment.bottomLeft
          : OverlayAlignment.bottomRight,
      overlayWidget: ValueListenableBuilder<String>(
        valueListenable: passwordNotifier,
        builder: (context, password, child) => PasswordHelperWidget(
          password: password,
        ),
      ),
      child: SizedBox(
        height: 80,
        key: passwordKey,
        child: CustomTextField(
          controller: widget.controller,
          focusNode: focusNode,
          maxLength: 50,
          onTap: widget.onTap,
          label: widget.label ?? context.getStrings.password_hint,
          readOnly: widget.readOnly ?? false,
          isObscure: !passwordVisible,
          textInputType: TextInputType.text,
          initValue: widget.initValue,
          suffixIcon: InkWell(
            onTap: () {
              passwordVisible = !passwordVisible;
              setState(() {});
            },
            child: passwordVisible
                ? Icon(
                    Icons.remove_red_eye_outlined,
                    color: context.colorScheme.tertiary,
                  )
                : Icon(
                    Icons.visibility_off_outlined,
                    color: context.colorScheme.tertiary,
                  ),
          ),
          onChanged: (value) {
            widget.onPasswordFieldChanged(value);
            passwordNotifier.value = value;
            setState(() {});
          },
        ),
      ),
    );
  }
}
