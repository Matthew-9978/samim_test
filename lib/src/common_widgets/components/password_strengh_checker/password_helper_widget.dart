import 'package:flutter/material.dart';
import 'package:samim_test/src/common_widgets/widgets/text_widget.dart';
import 'package:samim_test/src/utils/extesnsions/context_extensions.dart';
import 'package:samim_test/src/utils/regexes.dart';

class PasswordHelperWidget extends StatelessWidget {
  final String password;

  const PasswordHelperWidget({
    Key? key,
    required this.password,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 220,
      height: 140,
      child: Stack(
        children: [
          Positioned(
            right: context.isRTL ? null : 20,
            left: context.isRTL ? 20 : null,
            top: 5,
            child: RotationTransition(
              turns: const AlwaysStoppedAnimation(0.90),
              child: Container(
                width: 24,
                height: 24,
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.onSecondary,
                ),
              ),
            ),
          ),
          Positioned(
            top: 10,
            child: Container(
              width: 220,
              decoration: BoxDecoration(
                color: context.colorScheme.onSecondary,
                borderRadius: BorderRadius.circular(16),
              ),
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    PasswordConditionWidget(
                      regExp: Regexes.passwordLength(),
                      title: context.getStrings.password_eight_Characters,
                      password: password,
                    ),
                    const SizedBox(height: 8),
                    PasswordConditionWidget(
                      regExp: Regexes.camelCaseAlphabet(),
                      title: context.getStrings.password_lower_case,
                      password: password,
                    ),
                    const SizedBox(height: 8),
                    PasswordConditionWidget(
                      regExp: Regexes.smallCaseAlphabet(),
                      title: context.getStrings.password_upper_case,
                      password: password,
                    ),
                    const SizedBox(height: 8),
                    PasswordConditionWidget(
                      regExp: Regexes.latinCharacters(),
                      title: context.getStrings.password_latin_characters,
                      password: password,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PasswordConditionWidget extends StatelessWidget {
  final RegExp regExp;
  final String title;
  final String password;

  const PasswordConditionWidget({
    Key? key,
    required this.regExp,
    required this.title,
    required this.password,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Visibility(
          visible: regExp.hasMatch(password),
          child: Row(
            children: [
              Icon(
                Icons.check,
                size: 15,
                color: context.colorScheme.surfaceVariant,
              ),
              const SizedBox(width: 8,)
            ],
          ),
        ),
        Expanded(
          child: TextWidget.regular(
            title,
            context: context,
            maxLines: 1,
            additionalStyle: TextStyle(
              fontSize: 12,
              color: regExp.hasMatch(password)
                  ? context.colorScheme.surfaceVariant
                  : context.colorScheme.onTertiary,
            ),
          ),
        ),
      ],
    );
  }
}
