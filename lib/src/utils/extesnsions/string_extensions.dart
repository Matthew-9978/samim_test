import 'package:samim_test/src/utils/regexes.dart';

extension StringExtensions on String{
  bool isValidEmail(){
    return Regexes.email().hasMatch(this);
  }

  bool isValidPassword(){
    return Regexes.strengthPassword().hasMatch(this);
  }

}