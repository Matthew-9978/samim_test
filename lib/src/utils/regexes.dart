class Regexes {
  static RegExp strengthPassword() => RegExp(r'(?=.*[a-z])(?=.*[A-Z])[a-zA-Z]{8,}$');

  static RegExp passwordLength() => RegExp(r'^\S*(?=\S{8,})\S*$');

  static RegExp camelCaseAlphabet() => RegExp(r'(?=\S*[A-Z])\S*$');

  static RegExp smallCaseAlphabet() => RegExp(r'(?=\S*[a-z])\S*$');

  static RegExp latinCharacters() => RegExp(r'^[a-zA-Z]+$');

  static RegExp email() => RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

}