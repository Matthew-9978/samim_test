import 'package:flutter/material.dart';
import 'package:samim_test/src/utils/regexes.dart';

void postFrameCallback(VoidCallback callback) {
  WidgetsBinding.instance.addPostFrameCallback((timeStamp) => callback.call());
}
