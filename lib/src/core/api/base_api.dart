import 'package:flutter/cupertino.dart';
import 'package:kiwi/kiwi.dart';
import 'package:samim_test/src/core/network/api_provider.dart';

abstract class BaseApi {
  @protected
  final apiService = KiwiContainer().resolve<ApiService>();
}
