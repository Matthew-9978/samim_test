import 'package:kiwi/kiwi.dart';
import 'package:samim_test/src/core/bloc/preferences_cubit/preferences_cubit.dart';
import 'package:samim_test/src/core/storage/hive/hive_dependency.dart';

class PresentationModule {
  static Future<void> inject() async {
    KiwiContainer().registerSingleton<PreferencesCubit>(
        (container) => PreferencesCubit(localStorage: container.resolve()));
  }
}
