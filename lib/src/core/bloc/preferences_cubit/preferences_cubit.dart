import 'package:flutter/material.dart';
import 'package:samim_test/src/constants/light_theme.dart';
import 'package:samim_test/src/constants/tags.dart';
import 'package:samim_test/src/core/bloc/base_cubit/base_cubit.dart';
import 'package:samim_test/src/core/bloc/preferences_cubit/preferences_states.dart';
import 'package:samim_test/src/core/models/prefrences/entity/preferences_entity.dart';
import 'package:samim_test/src/core/storage/local_storage.dart';

class PreferencesCubit extends BaseCubit<PreferencesStates> {
  LocalStorage localStorage;

  PreferencesCubit({
    required this.localStorage,
  }) : super(PreferencesInitState());

  ThemeData theme = lightTheme;
  Locale locale = const Locale('en', 'US');

  void initialTheme() async {
    PreferencesEntity? preferencesEntity =
        await localStorage.read(Tags.preferencesBoxKey);
  }
}
