import 'package:freezed_annotation/freezed_annotation.dart';

part 'preferences_states.freezed.dart';

abstract class PreferencesStates {}

class PreferencesInitState extends PreferencesStates {}

@freezed
class ChangeThemeState extends PreferencesStates {
  const factory ChangeThemeState.dark() = DarkThemeState;

  const factory ChangeThemeState.light() = LightThemeState;
}

@freezed
class ChangeLanguageState extends PreferencesStates {
  const factory ChangeLanguageState.english() = EnglishLanguageState;

  const factory ChangeLanguageState.persian() = PersianLanguageState;
}
