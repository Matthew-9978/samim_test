import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kiwi/kiwi.dart';
import 'package:samim_test/src/core/models/api_result/result_model.dart';
import 'package:samim_test/src/core/error/exception.dart';

class BaseCubit<T> extends Cubit<T> {
  BaseCubit(T initialState) : super(initialState);

  void onViewCreated(){}

  final di = KiwiContainer();

  Future<void> safeCall<D>({
    required Future<ResultModel<D>> apiCall,
    Function(FailedResultStatus? failedStatus, String? error)? onFailed,
    Function(ResultModel<D>? result)? onSuccess,
  }) async {
    try {
      final response = await apiCall;
      if (response.isFailure) {
        onFailed?.call(response.failedStatus, response.error);
      } else {
        onSuccess?.call(response);
      }
    } on DioError catch (error) {
      if (error.error is SamimException) {
        addError(error.error.toString());
        onFailed?.call(null, error.error.toString());
      } else {
        onFailed?.call(null, error.message);
      }
    }
  }

  @override
  void emit(T state) {
    if (isClosed) return;
    super.emit(state);
  }

}
