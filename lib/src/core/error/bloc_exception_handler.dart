import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kiwi/kiwi.dart';
import 'package:samim_test/src/core/error/exception.dart';

class BlocExceptionHandler extends BlocObserver {
  BlocExceptionHandler();

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    if (error is UnauthorizedException) {
    }

    if (error is AccessDeniedException) {}

    super.onError(bloc, error, stackTrace);
  }
}
