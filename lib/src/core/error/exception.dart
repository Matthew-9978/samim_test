abstract class SamimException implements Exception {
  final String _message;
  final String _prefix;

  SamimException(this._message, this._prefix);

  @override
  String toString() {
    return _prefix + _message;
  }
}

class CompleteProfileException extends SamimException {
  CompleteProfileException({required String message})
      : super(message, "Complete profile : ");
}

class AccessDeniedException extends SamimException {
  AccessDeniedException({required String message})
      : super(message, "Access denied : ");
}

class UnauthorizedException extends SamimException {
  UnauthorizedException({required String message})
      : super(message, "UnAuthorized Exception : ");
}


