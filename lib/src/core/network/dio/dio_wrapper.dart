import 'dart:io';
import 'package:dio/dio.dart';
import 'package:samim_test/src/core/error/exception.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';


class DioWrapper {
  static Dio provide() {
    final dio = Dio();
    dio.interceptors.add(_getResponseInterceptor());
    dio.interceptors.add(_getLoggerInterceptor());
    return dio;
  }

  static QueuedInterceptor _getResponseInterceptor() {
    return QueuedInterceptorsWrapper(
      onRequest: _onRequest,
      onResponse: _onResponse,
      onError: _onError,
    );
  }

  static void _onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    options.headers["Accept"] = "application/json";
    options.headers["Content-type"] = "application/json";
    return handler.next(options);
  }

  static void _onResponse(
      Response response, ResponseInterceptorHandler handler) async {
    return handler.next(response);
  }

  static void _onError(
      DioError options, ErrorInterceptorHandler handler) async {
    print(options.response?.statusCode);
    print(options.response);
    if (options.error is SocketException) {
      handler.reject(DioError(requestOptions: options.requestOptions));
    }
    switch (options.response?.statusCode) {
      case HttpStatus.forbidden:
        return handler.reject(
          DioError(
            requestOptions: options.requestOptions,
            error: AccessDeniedException(message: options.message??""),
          ),
        );
      case 413:
        return handler.reject(
          DioError(
            requestOptions: options.requestOptions,
            error: UnauthorizedException(message: options.message??""),
          ),
        );
      case 401:
        return handler.reject(
          DioError(
            requestOptions: options.requestOptions,
            error: UnauthorizedException(message: options.message??""),
          ),
        );
      default:
        if(options.response != null){
          return handler.resolve(options.response!);
        }
        return handler.reject(
          DioError(
            requestOptions: options.requestOptions,
          ),
        );
    }
  }

  static PrettyDioLogger _getLoggerInterceptor() {
    return PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        compact: true,
        maxWidth: 150);
  }
}
