import 'package:dio/dio.dart';


class ApiResponse<T> {
  T? data;
  String? message;
  late int statusCode;
  String? error;

  ApiResponse({
    required this.data,
    required this.statusCode,
    this.error,
    this.message,
  });

  ApiResponse.fromResponse({
    required Response response,
    required T Function(dynamic)? resultMapper,
  }) {
    statusCode = response.statusCode!;
    data = isSuccess ? resultMapper?.call(response.data) : null;
    error = _extractError(response);
  }

  bool get isSuccess {
    return statusCode >= 200 && statusCode <= 299;
  }

  bool get isNotFound {
    return statusCode == 404;
  }

  dynamic _extractError(Response<dynamic> response) {
    if (isSuccess) return null;
    Map? errorList;
    String error = "";
    if (response.data.toString().isEmpty) return null;
    if (response.data['error'] != null) {
      errorList = response.data["error"];
      errorList?.forEach((key, value) {
        error += "$value\n";
      });
    }
    return error;
  }


}
