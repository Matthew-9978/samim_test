import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'prefrence_model.freezed.dart';

@unfreezed
class PreferencesModel extends Equatable with _$PreferencesModel {
  const PreferencesModel._();

  factory PreferencesModel({
    String? language,
    String? locale,
    String? theme,
  }) = _PreferencesModel;

  factory PreferencesModel.clone(PreferencesModel model) => PreferencesModel(
        language: model.language,
        locale: model.locale,
        theme: model.theme,
      );

  @override
  List<Object?> get props => [language, locale, theme];
}
