import 'package:samim_test/src/core/bloc/presentation_module.dart';
import 'package:samim_test/src/core/network/network_module.dart';
import 'package:samim_test/src/core/storage/storage_module.dart';
import 'package:samim_test/src/features/authentication/auth_injector.dart';
import 'package:samim_test/src/features/countries/countries_injector.dart';

class Injector {
  static inject() async {
    await StorageModule.inject();
    await NetworkModule.inject();
    await PresentationModule.inject();
    await AuthDependencies.inject();
    await CountriesDependencies.inject();
  }
}