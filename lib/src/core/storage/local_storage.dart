import 'dart:async';

abstract class LocalStorage {
  Future<void> clearAll();

  Future<void> write(String key,dynamic object);

  Future<dynamic> read(String key);

  Future<void> clearByKey(String key);
}
