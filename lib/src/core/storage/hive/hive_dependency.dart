import 'package:hive_flutter/hive_flutter.dart';
import 'package:kiwi/kiwi.dart';
import 'package:samim_test/src/constants/tags.dart';
import 'package:samim_test/src/core/models/prefrences/entity/preferences_entity.dart';
import 'package:samim_test/src/core/storage/hive/hive_local_storage_impl.dart';
import 'package:samim_test/src/core/storage/local_storage.dart';

class HiveDependency {
  static Future<void> inject() async {
    await _injectSingletons();
  }

  static Future<void> _injectSingletons() async {
    await _injectAndInitHive();
    await _openHiveBoxes();
  }

  static Future<void> _openHiveBoxes() async {
    await Hive.openBox(Tags.preferencesBoxKey);
  }

  static Future<void> _injectAndInitHive() async {
    await Hive.initFlutter();
    Hive.registerAdapter(PreferencesEntityAdapter());
    KiwiContainer()
        .registerSingleton<LocalStorage>((container) => HiveLocalStorageImpl());
  }
}
