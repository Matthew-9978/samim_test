import 'package:hive_flutter/hive_flutter.dart';
import 'package:samim_test/src/core/storage/local_storage.dart';

class HiveLocalStorageImpl extends LocalStorage {
  @override
  Future<void> clearAll() async {
    await Hive.deleteFromDisk();
  }

  @override
  Future<dynamic> read(String key) async {
    final box = await Hive.openBox(key);
    return box.get(key);
  }

  @override
  Future<void> write(String key, object) async {
    final box = await Hive.openBox(key);
    await box.put(key, object);
  }

  @override
  Future<void> clearByKey(String key) async {
    final userBox = await Hive.openBox(key);
    await userBox.deleteFromDisk();
  }
}
