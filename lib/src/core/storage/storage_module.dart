import 'package:samim_test/src/core/storage/hive/hive_dependency.dart';

class StorageModule {
  static Future<void> inject() async {
    await HiveDependency.inject();
  }
}
