abstract class Routes {
  Routes._();

  static const login = _Paths.login;
  static const countryList = _Paths.countryList;
}

abstract class _Paths {
  static const login = '/';
  static const countryList = '/countries';
}
