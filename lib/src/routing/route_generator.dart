import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kiwi/kiwi.dart';
import 'package:samim_test/src/features/authentication/presentation/bloc/login_cubit/login_cubit.dart';
import 'package:samim_test/src/features/authentication/presentation/pages/login_page/login_page.dart';
import 'package:samim_test/src/features/countries/presentation/bloc/country_list_bloc/country_list_cubit.dart';
import 'package:samim_test/src/features/countries/presentation/pages/country_list_page/countrye_list_page.dart';
import 'package:samim_test/src/routing/routes.dart';

class RouteGenerator {
  static Map<String, WidgetBuilder> getRoutes(RouteSettings settings) {
    dynamic param = settings.arguments;
    KiwiContainer di = KiwiContainer();

    return {
      Routes.login: (context) => BlocProvider(
            create: (context) => LoginCubit(
              loginRepository: di.resolve(),
            ),
            child: const LoginPage(),
          ),
      Routes.countryList: (context) => BlocProvider(
            create: (context) => CountryListCubit(
              countryRepository: di.resolve(),
            ),
            child: const CountryListPage(),
          ),
    };
  }
}
