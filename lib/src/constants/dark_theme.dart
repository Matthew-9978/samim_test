import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:samim_test/src/constants/assets.dart';
import 'package:samim_test/src/constants/colors.dart';

ThemeData get darkTheme {
  return ThemeData.light().copyWith(
    brightness: Brightness.light,
    primaryColor: PrimaryColor.dark,
    dividerColor: Colors.transparent,
    scaffoldBackgroundColor: BackgroundColor.dark,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: TextPrimaryColor.dark,
      selectionColor: PrimaryColor.dark,
      selectionHandleColor: PrimaryColor.dark,
    ),
    bottomSheetTheme: BottomSheetThemeData(
      backgroundColor: BackgroundColor.dark,
    ),
    appBarTheme: AppBarTheme(
      systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
        statusBarColor: BackgroundColor.dark,
        statusBarBrightness: Brightness.light,
      ),
      elevation: 0,
      backgroundColor: BackgroundColor.dark,
    ),
    colorScheme: ColorScheme.light(
      brightness: Brightness.dark,
      primary: PrimaryColor.dark,
      error: PrimaryColor.dark,
      background: BackgroundColor.dark,
      onBackground: BackgroundSecondaryColor.dark,
      onSecondary: BackgroundTertiaryColor.dark,
      tertiary: TextPrimaryColor.dark,
      onTertiary: TextSubtitleColor.dark,
      tertiaryContainer: DisabledTextColor.dark,
      onTertiaryContainer: TextButtonColor.dark,
      surfaceTint: DisabledColor.dark,
      outline: BorderColor.dark,
      surface: YellowColor.dark,
      onSurface: BlueColor.dark,
      surfaceVariant: GreenColor.dark,
      onSurfaceVariant: ActiveBorderColor.dark,
      inverseSurface: HoverColor.dark,
      onPrimary: SecondaryRed.dark,
    ),
    textTheme: TextTheme(
      headlineLarge: TextStyle(
          fontSize: 28,
          fontFamily: Assets.fontName,
          color: TextPrimaryColor.dark,
          fontWeight: FontWeight.w700,
          fontFamilyFallback: const [Assets.fontName]),
      headlineMedium: TextStyle(
          fontSize: 24,
          fontFamily: Assets.fontName,
          color: TextPrimaryColor.dark,
          fontWeight: FontWeight.w700,
          fontFamilyFallback: const [Assets.fontName]),
      headlineSmall: TextStyle(
          fontSize: 20,
          fontFamily: Assets.fontName,
          color: TextPrimaryColor.dark,
          fontWeight: FontWeight.w700,
          fontFamilyFallback: const [Assets.fontName]),
      bodyLarge: TextStyle(
          fontSize: 20,
          fontFamily: Assets.fontName,
          color: TextPrimaryColor.dark,
          fontWeight: FontWeight.w500,
          fontFamilyFallback: const [Assets.fontName]),
      bodyMedium: TextStyle(
          fontSize: 16,
          fontFamily: Assets.fontName,
          color: TextPrimaryColor.dark,
          fontWeight: FontWeight.w500,
          fontFamilyFallback: const [Assets.fontName]),
      bodySmall: TextStyle(
          fontSize: 12,
          fontFamily: Assets.fontName,
          color: TextPrimaryColor.dark,
          fontWeight: FontWeight.w500,
          fontFamilyFallback: const [Assets.fontName]),
    ),
  );
}
