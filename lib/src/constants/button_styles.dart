import 'package:flutter/material.dart';
import 'package:samim_test/src/utils/extesnsions/context_extensions.dart';

class ButtonStyles {
  static ButtonStyle fill({
    required BuildContext context,
    Color? backgroundColor,
    Color? foregroundColor,
    TextStyle? additionalTextStyle,
    double? borderRadius,
  }) =>
      ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (states) {
            if (states.contains(MaterialState.disabled)) {
              return Theme.of(context).colorScheme.surfaceTint;
            }
            return backgroundColor ?? Theme.of(context).colorScheme.primary;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith((state) {
          if (state.contains(MaterialState.disabled)) {
            return Theme.of(context).colorScheme.tertiaryContainer;
          }
          return foregroundColor ??
              Theme.of(context).colorScheme.onTertiaryContainer;
        }),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius ?? 1000),
          ),
        ),
        padding: MaterialStateProperty.all(EdgeInsets.zero),
        overlayColor: MaterialStateColor.resolveWith(
              (states) => Theme.of(context).colorScheme.inverseSurface,
        ),
        elevation: MaterialStateProperty.all<double>(0),
        textStyle: MaterialStateProperty.resolveWith(
              (state) {
            if (state.contains(MaterialState.disabled)) {
              return context.textTheme.bodyMedium!
                  .copyWith(
                  color: Theme.of(context).colorScheme.tertiaryContainer,
                  fontSize: 18.0)
                  .merge(additionalTextStyle);
            }
            return context.textTheme.bodyMedium!
                .copyWith(
                color: Theme.of(context).colorScheme.onTertiaryContainer,
                fontSize: 18.0)
                .merge(additionalTextStyle);
          },
        ),
      );


}
