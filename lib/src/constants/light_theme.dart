import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:samim_test/src/constants/assets.dart';
import 'package:samim_test/src/constants/colors.dart';

ThemeData get lightTheme {
  return ThemeData.light().copyWith(
    brightness: Brightness.light,
    primaryColor: PrimaryColor.light,
    dividerColor: Colors.transparent,
    scaffoldBackgroundColor: BackgroundColor.light,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: TextPrimaryColor.light,
      selectionColor: PrimaryColor.light,
      selectionHandleColor: PrimaryColor.light,
    ),
    bottomSheetTheme: BottomSheetThemeData(
      backgroundColor: BackgroundColor.light,
    ),
    appBarTheme: AppBarTheme(
      systemOverlayStyle: SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: BackgroundColor.light,
        statusBarBrightness: Brightness.light,
      ),
      elevation: 0,
      backgroundColor: BackgroundColor.light,
    ),
    colorScheme: ColorScheme.light(
      brightness: Brightness.light,
      primary: PrimaryColor.light,
      error: PrimaryColor.light,
      background: BackgroundColor.light,
      onBackground: BackgroundSecondaryColor.light,
      onSecondary: BackgroundTertiaryColor.light,
      tertiary: TextPrimaryColor.light,
      onTertiary: TextSubtitleColor.light,
      tertiaryContainer: DisabledTextColor.light,
      onTertiaryContainer: TextButtonColor.light,
      surfaceTint: DisabledColor.light,
      outline: BorderColor.light,
      surface: YellowColor.light,
      onSurface: BlueColor.light,
      surfaceVariant: GreenColor.light,
      onSurfaceVariant: ActiveBorderColor.light,
      inverseSurface: HoverColor.light,
      onPrimary: SecondaryRed.light,
    ),
    textTheme: TextTheme(
      headlineLarge: TextStyle(
          fontSize: 28,
          fontFamily: Assets.fontName,
          color: TextPrimaryColor.light,
          fontWeight: FontWeight.w700,
          fontFamilyFallback: const [Assets.fontName]),
      headlineMedium: TextStyle(
          fontSize: 24,
          fontFamily: Assets.fontName,
          color: TextPrimaryColor.light,
          fontWeight: FontWeight.w700,
          fontFamilyFallback: const [Assets.fontName]),
      headlineSmall: TextStyle(
          fontSize: 20,
          fontFamily: Assets.fontName,
          color: TextPrimaryColor.light,
          fontWeight: FontWeight.w700,
          fontFamilyFallback: const [Assets.fontName]),
      bodyLarge: TextStyle(
        fontSize: 20,
        fontFamily: Assets.fontName,
        color: TextPrimaryColor.light,
        fontWeight: FontWeight.w500,
        fontFamilyFallback: const [Assets.fontName]),
      bodyMedium: TextStyle(
          fontSize: 16,
          fontFamily: Assets.fontName,
          color: TextPrimaryColor.light,
          fontWeight: FontWeight.w500,
          fontFamilyFallback: const [Assets.fontName]),
      bodySmall: TextStyle(
        fontSize: 12,
        fontFamily: Assets.fontName,
        color: TextPrimaryColor.light,
        fontWeight: FontWeight.w500,
        fontFamilyFallback: const [Assets.fontName]),
    ),
  );
}
