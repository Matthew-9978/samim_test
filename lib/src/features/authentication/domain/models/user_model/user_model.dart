import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:samim_test/src/features/authentication/data/entity/user_entity/user_entity.dart';

part 'user_model.freezed.dart';

@unfreezed
class UserModel extends Equatable with _$UserModel {
  const UserModel._();

  factory UserModel({
    String? username,
    String? email,
    UserStateEnum? state,
  }) = _UserModel;

  factory UserModel.clone(UserModel model) => UserModel(
    username: model.username,
    email: model.email,
    state: model.state,
  );

  @override
  List<Object?> get props => [username, email, state];
}
