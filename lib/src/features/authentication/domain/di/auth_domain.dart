import 'package:kiwi/kiwi.dart';
import 'package:samim_test/src/features/authentication/data/repository/login_repository_impl.dart';
import 'package:samim_test/src/features/authentication/domain/repository/login_repository/login_repository.dart';

class AuthDomain {
  static Future<void> inject() async {
    KiwiContainer().registerFactory<LoginRepository>(
        (container) => LoginRepositoryImpl(loginApi: container.resolve()));
  }
}
