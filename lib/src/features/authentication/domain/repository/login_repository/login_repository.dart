import 'package:samim_test/src/core/models/api_result/result_model.dart';
import 'package:samim_test/src/features/authentication/domain/models/user_model/user_model.dart';

abstract class LoginRepository {
  Future<ResultModel<List<UserModel>>> getUsers();
}