import 'package:samim_test/src/features/authentication/data/di/auth_data.dart';
import 'package:samim_test/src/features/authentication/domain/di/auth_domain.dart';
import 'package:samim_test/src/features/authentication/presentation/di/auth_presentation.dart';

class AuthDependencies {
  static inject() async {
    await _injectData();
    await _injectDomain();
    await _injectPresentation();
  }

  static Future<void> _injectData() async {
    await AuthData.inject();
  }

  static Future<void> _injectDomain() async {
    await AuthDomain.inject();
  }

  static Future<void> _injectPresentation() async {
    await AuthPresentation.inject();
  }
}