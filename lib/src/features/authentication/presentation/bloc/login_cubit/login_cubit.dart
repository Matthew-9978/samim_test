import 'package:samim_test/src/core/bloc/base_cubit/base_cubit.dart';
import 'package:samim_test/src/features/authentication/data/entity/user_entity/user_entity.dart';
import 'package:samim_test/src/features/authentication/domain/models/user_model/user_model.dart';
import 'package:samim_test/src/features/authentication/domain/repository/login_repository/login_repository.dart';
import 'package:samim_test/src/features/authentication/presentation/bloc/login_cubit/login_state.dart';
import 'package:samim_test/src/utils/extesnsions/string_extensions.dart';

class LoginCubit extends BaseCubit<LoginState> {
  LoginRepository loginRepository;

  LoginCubit({
    required this.loginRepository,
  }) : super(LoginInitial());

  void validateEmailAddress(String email) {
    if (email.isValidEmail()) {
      emit(const EmailValidationState.valid());
    } else {
      emit(const EmailValidationState.invalid());
    }
  }

  void validatePassword(String password) {
    if (password.isValidPassword()) {
      emit(const PasswordValidationState.valid());
    } else {
      emit(const PasswordValidationState.invalid());
    }
  }

  bool validateLoginForm(String email, String password) {
    if (password.isValidPassword() && email.isValidEmail()) {
      return true;
    }
    return false;
  }

  Future<void> getUsers({
    required String email,
    required String password,
  }) async {
    emit(const GetUsersState.loading());
    await safeCall(
      apiCall: loginRepository.getUsers(),
      onSuccess: (result) {
        if (result?.data != null) {
          for (UserModel userModel in result!.data!) {
            if (userModel.email != email) continue;
            if (userModel.state == UserStateEnum.enable) {
              emit(const GetUsersState.success());
            } else {
              emit(const GetUsersState.failed());
            }
            return;
          }
        }
        emit(const GetUsersState.failed());
      },
      onFailed: (failedStatus, error) {
        emit(GetUsersState.failed(error: error));
      },
    );
  }
}
