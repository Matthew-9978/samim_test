import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_state.freezed.dart';

abstract class LoginState {}

class LoginInitial extends LoginState {}

@freezed
class EmailValidationState extends LoginState {
  const factory EmailValidationState.valid() = ValidEmailState;

  const factory EmailValidationState.invalid() = InvalidEmailState;
}

@freezed
class PasswordValidationState extends LoginState {
  const factory PasswordValidationState.valid() = ValidPasswordState;

  const factory PasswordValidationState.invalid() = InvalidPasswordState;
}

@freezed
class GetUsersState extends LoginState with _$GetUsersState {
  const factory GetUsersState.loading() = LoadingGetUsersState;

  const factory GetUsersState.success() = SUccessGetUsersState;

  const factory GetUsersState.failed({String? error}) = FailedGetUsersState;
}
