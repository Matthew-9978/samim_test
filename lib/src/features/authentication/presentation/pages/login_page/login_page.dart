import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:samim_test/src/common_widgets/abstracts/base_stateful_widget.dart';
import 'package:samim_test/src/common_widgets/components/password_strengh_checker/password_input_widget.dart';
import 'package:samim_test/src/common_widgets/widgets/button_widget.dart';
import 'package:samim_test/src/common_widgets/widgets/image_widget.dart';
import 'package:samim_test/src/common_widgets/widgets/text_field_widget.dart';
import 'package:samim_test/src/constants/assets.dart';
import 'package:samim_test/src/features/authentication/presentation/bloc/login_cubit/login_cubit.dart';
import 'package:samim_test/src/features/authentication/presentation/bloc/login_cubit/login_state.dart';
import 'package:samim_test/src/routing/routes.dart';
import 'package:samim_test/src/utils/extesnsions/context_extensions.dart';
import 'package:samim_test/src/utils/extesnsions/dart_extension.dart';

class LoginPage extends BaseStatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends BaseStatefulWidgetState<LoginPage, LoginCubit> {
  late TextEditingController emailController;
  late TextEditingController passwordController;

  @override
  void initState() {
    super.initState();
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget body(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const ImageLoaderWidget(
            imageUrl: Assets.logo,
            height: 200,
            width: 200,
          ),
          SizedBox(
            height: context.heightPercentage(20),
          ),
          BlocBuilder<LoginCubit, LoginState>(
            buildWhen: _buildWhenEmailInput,
            builder: (context, state) => CustomTextField(
              controller: emailController,
              label: context.getStrings.email,
              errorText: state is InvalidEmailState
                  ? context.getStrings.invalid_email
                  : null,
              onChanged: (value) {
                cubit.validateEmailAddress(value);
              },
            ),
          ),
          const SizedBox(
            height: 16,
          ),
          PasswordInputWidget(
            controller: passwordController,
            onPasswordFieldChanged: (value) {
              cubit.validatePassword(value);
            },
            showPasswordHelper: true,
            focusOnCreate: false,
          ),
          const SizedBox(
            height: 24,
          ),
          BlocConsumer<LoginCubit, LoginState>(
            listener: _listenToGetUsersState,
            listenWhen: _listenWhenToGetUsersState,
            buildWhen: _buildWhenLoginButton,
            builder: (context, state) => CustomButton.fill(
              context: context,
              text: context.getStrings.login,
              loadingType: ButtonLoadingType.percentage,
              loadingStatus: _buttonLoadingStatus(state),
              isEnable: cubit.validateLoginForm(
                  emailController.text, passwordController.text),
              onPressed: () {
                cubit.getUsers(
                  email: emailController.text,
                  password: passwordController.text,
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  ButtonLoadingStatus _buttonLoadingStatus(LoginState state) {
    if (state is LoadingGetUsersState) {
      return ButtonLoadingStatus.loading;
    } else if (state is SUccessGetUsersState) {
      return ButtonLoadingStatus.complete;
    }
    return ButtonLoadingStatus.normal;
  }

  bool _buildWhenEmailInput(LoginState previous, LoginState current) {
    return current is EmailValidationState;
  }

  bool _buildWhenLoginButton(LoginState previous, LoginState current) {
    return current is EmailValidationState ||
        current is PasswordValidationState || current is GetUsersState;
  }

  bool _listenWhenToGetUsersState(LoginState previous, LoginState current) {
    return current is GetUsersState;
  }

  void _listenToGetUsersState(BuildContext context, LoginState state) {
    state.isA<GetUsersState>()?.whenOrNull(
      success: (){
        Navigator.pushNamed(context, Routes.countryList);
      },
      failed: (error) {
        context.showFailedSnackBar(context.getStrings.no_login_permission);
      },
    );
  }
}
