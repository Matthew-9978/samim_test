import 'package:hive_flutter/adapters.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_entity.g.dart';

enum UserStateEnum { enable, disable }

@JsonSerializable()
class UserEntity {
  @JsonKey(name: 'username')
  String? username;
  @JsonKey(name: 'email')
  String? email;
  @JsonKey(name: 'state')
  UserStateEnum? state;

  UserEntity({
    this.username,
    this.email,
    this.state,
  });

  factory UserEntity.fromJson(Map<String, dynamic> json) =>
      _$UserEntityFromJson(json);

  Map<String, dynamic> toJson() => _$UserEntityToJson(this);
}
