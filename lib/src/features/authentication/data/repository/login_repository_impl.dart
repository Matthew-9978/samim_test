import 'package:samim_test/src/core/models/api_result/result_model.dart';
import 'package:samim_test/src/features/authentication/data/api/login_api/login_api.dart';
import 'package:samim_test/src/features/authentication/data/mapper/user_entity_mapper.dart';
import 'package:samim_test/src/features/authentication/domain/models/user_model/user_model.dart';
import 'package:samim_test/src/features/authentication/domain/repository/login_repository/login_repository.dart';

class LoginRepositoryImpl extends LoginRepository {
  LoginApi loginApi;

  LoginRepositoryImpl({
    required this.loginApi,
  });

  @override
  Future<ResultModel<List<UserModel>>> getUsers() async {
    final result = await loginApi.getUsers();
    return ResultModel(
      data: result.data?.map((userEntity) => userEntity.mapToModel()).toList(),
      status: ResultStatus.success,
      statusCode: result.statusCode,
    );

  }
}
