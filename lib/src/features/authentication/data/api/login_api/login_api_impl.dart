import 'dart:convert';

import 'package:samim_test/src/core/models/api_response/api_response.dart';
import 'package:samim_test/src/features/authentication/data/api/login_api/login_api.dart';
import 'package:samim_test/src/features/authentication/data/entity/user_entity/user_entity.dart';

class LoginApiImpl extends LoginApi {
  @override
  Future<ApiResponse<List<UserEntity>>> getUsers() async {
    final response = jsonDecode(users);
    await Future.delayed(const Duration(seconds: 3));
    return ApiResponse(
      data: List<UserEntity>.from(
          response.map((json) => UserEntity.fromJson(json))),
      statusCode: 200,
    );
  }
}

String users = '''  
[{"username":"mina","email":"mina@gmail.com","state":"enable"},{"username":"arash","email":"arash@gmail.com","state":"disable"},{"username":"sara","email":"sara@gmail.com","state":"disable"},{"username":"milad","email":"milad@gmail.com","state":"enable"}]
''';
