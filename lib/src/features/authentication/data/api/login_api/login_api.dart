import 'package:samim_test/src/core/models/api_response/api_response.dart';
import 'package:samim_test/src/features/authentication/data/entity/user_entity/user_entity.dart';

abstract class LoginApi{
  Future<ApiResponse<List<UserEntity>>> getUsers();
}