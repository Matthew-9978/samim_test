import 'package:samim_test/src/features/authentication/data/entity/user_entity/user_entity.dart';
import 'package:samim_test/src/features/authentication/domain/models/user_model/user_model.dart';

extension UserEntityMapper on UserEntity {
  UserModel mapToModel() {
    return UserModel(
      email: email,
      username: username,
      state: state,
    );
  }
}
