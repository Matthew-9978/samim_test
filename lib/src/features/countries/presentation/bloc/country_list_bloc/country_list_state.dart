import 'package:freezed_annotation/freezed_annotation.dart';
part 'country_list_state.freezed.dart';

abstract class CountryListState {}

class CountryListInitial extends CountryListState {}

@freezed
class GetCountriesList extends CountryListState with _$GetCountriesList {
  const factory GetCountriesList.loading() = LoadingGetCountriesList;

  const factory GetCountriesList.success() = SUccessGetCountriesList;

  const factory GetCountriesList.failed({String? error}) = FailedGetCountriesList;
}

