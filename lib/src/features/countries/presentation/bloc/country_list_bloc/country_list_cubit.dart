import 'package:samim_test/src/core/bloc/base_cubit/base_cubit.dart';
import 'package:samim_test/src/features/countries/domain/models/country_model/country_model.dart';
import 'package:samim_test/src/features/countries/domain/repository/country_repository/country_repository.dart';
import 'package:samim_test/src/features/countries/presentation/bloc/country_list_bloc/country_list_state.dart';

class CountryListCubit extends BaseCubit<CountryListState> {
  CountryRepository countryRepository;

  CountryListCubit({
    required this.countryRepository,
  }) : super(CountryListInitial());

  List<CountryModel> countries = [];

  @override
  void onViewCreated() {
    getCountries();
  }

  List<CountryModel> loadMoreCountries(int length)  {
    List<CountryModel> list = [];
    if (countries.length - length < 20) {
      list = countries.getRange(length - 1, countries.length).toList();
    }
    else{
      list = countries.getRange(length - 1, length + 20).toList();
    }
    return list;
  }

  Future<void> getCountries() async {
    emit(const GetCountriesList.loading());
    safeCall(
      apiCall: countryRepository.getCountries(),
      onSuccess: (result) {
        countries = result?.data ?? [];
        emit(const GetCountriesList.success());
      },
      onFailed: (failedStatus, error) {
        emit(GetCountriesList.failed(error: error));
      },
    );
  }
}
