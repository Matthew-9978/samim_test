import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:samim_test/src/common_widgets/abstracts/base_stateful_widget.dart';
import 'package:samim_test/src/common_widgets/widgets/app_bar_widget.dart';
import 'package:samim_test/src/common_widgets/widgets/error_widget.dart';
import 'package:samim_test/src/common_widgets/widgets/lazy_listview_widget.dart';
import 'package:samim_test/src/features/countries/domain/models/country_model/country_model.dart';
import 'package:samim_test/src/features/countries/presentation/bloc/country_list_bloc/country_list_cubit.dart';
import 'package:samim_test/src/features/countries/presentation/bloc/country_list_bloc/country_list_state.dart';
import 'package:samim_test/src/features/countries/presentation/pages/country_list_page/widgets/countries_item_widget.dart';
import 'package:samim_test/src/features/countries/presentation/pages/country_list_page/widgets/skeleton_loading.dart';
import 'package:samim_test/src/utils/extesnsions/context_extensions.dart';

class CountryListPage extends BaseStatefulWidget {
  const CountryListPage({Key? key}) : super(key: key);

  @override
  State<CountryListPage> createState() => _LoginPageState();
}

class _LoginPageState
    extends BaseStatefulWidgetState<CountryListPage, CountryListCubit> {

  @override
  CustomAppbar? appBar(BuildContext context) => CustomAppbar(
    title: context.getStrings.countries,
    hasBackButton: true,
    onPressBack: () {
      Navigator.pop(context);
    },
  );


  @override
  Widget body(BuildContext context) {
    return BlocBuilder<CountryListCubit,CountryListState>(
      buildWhen: _buildWhenGetCountries,
      builder: (context, state) {
        if (state is GetCountriesList) {
          return state.when(
            loading: () => const CountriesSkeletonLoading(),
            success: () => LazyListViewWidget<CountryModel>(
              items: cubit.countries.getRange(0, 20).toList(),
              itemBuilder: (item) => CountriesItemWidget(countryModel: item),
              onLoadMore: cubit.loadMoreCountries,
              separatorWidget: const SizedBox(height: 8,),
            ),
            failed: (error) => CommonErrorWidget(
              error: error,
            ),
          );
        }
        return const SizedBox();
      },
    );
  }

  bool _buildWhenGetCountries(Object? previous, Object? current) {
    return current is GetCountriesList;
  }
}
