import 'package:flutter/material.dart';
import 'package:samim_test/src/common_widgets/widgets/skeleton_widget.dart';
import 'package:samim_test/src/utils/extesnsions/context_extensions.dart';

class CountriesSkeletonLoading extends StatelessWidget {
  const CountriesSkeletonLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: 20,
      itemBuilder: (context, index) =>
          SkeletonWidget.rectangular(width: context.screenWidth, height: 80),
      separatorBuilder: (context, index) => const SizedBox(height: 8,),
    );
  }
}
