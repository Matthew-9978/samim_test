import 'package:flutter/material.dart';
import 'package:samim_test/src/common_widgets/widgets/image_widget.dart';
import 'package:samim_test/src/common_widgets/widgets/text_widget.dart';
import 'package:samim_test/src/features/countries/domain/models/country_model/country_model.dart';
import 'package:samim_test/src/utils/extesnsions/context_extensions.dart';

class CountriesItemWidget extends StatelessWidget {
  final CountryModel countryModel;

  const CountriesItemWidget({
    Key? key,
    required this.countryModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
      decoration: BoxDecoration(
        color: context.colorScheme.onBackground,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            offset: const Offset(0, 0),
            blurRadius: 10,
          )
        ],
      ),
      child: Row(
        children: [
          ImageLoaderWidget(
            imageUrl: countryModel.flag ?? "",
            height: 48,
            width: 48,
            boxShape: BoxShape.circle,
          ),
          const SizedBox(
            width: 12,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextWidget.semiBold(
                  countryModel.name ?? "",
                  context: context,
                  additionalStyle: const TextStyle(fontSize: 18),
                ),
                TextWidget.semiBold(
                  countryModel.capital ?? "",
                  context: context,
                  additionalStyle:  TextStyle(fontSize: 16,color: context.colorScheme.onTertiary),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
