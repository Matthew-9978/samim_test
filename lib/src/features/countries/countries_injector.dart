
import 'package:samim_test/src/features/countries/data/di/countries_data.dart';
import 'package:samim_test/src/features/countries/domain/di/countries_domain.dart';
import 'package:samim_test/src/features/countries/presentation/di/countries_presentation.dart';

class CountriesDependencies {
  static inject() async {
    await _injectData();
    await _injectDomain();
    await _injectPresentation();
  }

  static Future<void> _injectData() async {
    await CountriesData.inject();
  }

  static Future<void> _injectDomain() async {
    await CountriesDomain.inject();
  }

  static Future<void> _injectPresentation() async {
    await CountriesPresentation.inject();
  }
}