import 'package:samim_test/src/core/models/api_result/result_model.dart';
import 'package:samim_test/src/features/countries/domain/models/country_model/country_model.dart';

abstract class CountryRepository {
  Future<ResultModel<List<CountryModel>>> getCountries();
}
