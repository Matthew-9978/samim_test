import 'package:kiwi/kiwi.dart';
import 'package:samim_test/src/features/authentication/data/repository/login_repository_impl.dart';
import 'package:samim_test/src/features/authentication/domain/repository/login_repository/login_repository.dart';
import 'package:samim_test/src/features/countries/data/repository/country_repository_impl.dart';
import 'package:samim_test/src/features/countries/domain/repository/country_repository/country_repository.dart';

class CountriesDomain {
  static Future<void> inject() async {
    KiwiContainer().registerFactory<CountryRepository>(
        (container) => CountryRepositoryImpl(countryApi: container.resolve()));
  }
}
