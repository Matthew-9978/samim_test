import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'country_model.freezed.dart';

@unfreezed
class CountryModel extends Equatable with _$CountryModel {
  const CountryModel._();

  factory CountryModel({
    String? name,
    String? capital,
    String? region,
    int? population,
    String? flag,
  }) = _CountryModel;

  factory CountryModel.clone(CountryModel model) => CountryModel(
        name: model.name,
        capital: model.capital,
        region: model.region,
        population: model.population,
        flag: model.flag,
      );

  @override
  List<Object?> get props => [name, capital, region, population, flag];
}
