import 'package:kiwi/kiwi.dart';
import 'package:samim_test/src/features/authentication/data/api/login_api/login_api.dart';
import 'package:samim_test/src/features/authentication/data/api/login_api/login_api_impl.dart';
import 'package:samim_test/src/features/authentication/data/repository/login_repository_impl.dart';
import 'package:samim_test/src/features/authentication/domain/repository/login_repository/login_repository.dart';
import 'package:samim_test/src/features/countries/data/api/country_api/country_api.dart';
import 'package:samim_test/src/features/countries/data/api/country_api/country_api_impl.dart';

class CountriesData {
  static Future<void> inject() async {
    KiwiContainer().registerFactory<CountryApi>((container) => CountryApiImpl());
  }
}
