import 'dart:convert';

import 'package:samim_test/src/core/models/api_response/api_response.dart';
import 'package:samim_test/src/features/countries/data/api/country_api/country_api.dart';
import 'package:samim_test/src/features/countries/data/entity/country_entity/country_entity.dart';

class CountryApiImpl extends CountryApi {
  @override
  Future<ApiResponse<List<CountryEntity>>> getCountries() async {
    final response = await apiService.get(
      'https://raw.githubusercontent.com/esmaeil-ahmadipour/restcountries/main/json/countriesV2.json',
    );
    return ApiResponse.fromResponse(
      response: response,
      resultMapper: (data) => List<CountryEntity>.from(
          jsonDecode(data).map((json) => CountryEntity.fromJson(json))),
    );
  }
}
