import 'package:mockito/annotations.dart';
import 'package:samim_test/src/core/api/base_api.dart';
import 'package:samim_test/src/core/models/api_response/api_response.dart';
import 'package:samim_test/src/features/countries/data/entity/country_entity/country_entity.dart';

@GenerateMocks([CountryApi])
abstract class CountryApi extends BaseApi{
  Future<ApiResponse<List<CountryEntity>>> getCountries();
}