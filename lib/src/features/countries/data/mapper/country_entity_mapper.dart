import 'package:samim_test/src/features/countries/data/entity/country_entity/country_entity.dart';
import 'package:samim_test/src/features/countries/domain/models/country_model/country_model.dart';

extension CountryEntityMapper on CountryEntity {
  CountryModel mapToModel() {
    return CountryModel(
      name: name,
      capital: capital,
      flag: flag,
      population: population,
      region: region,
    );
  }
}
