import 'package:hive_flutter/adapters.dart';
import 'package:json_annotation/json_annotation.dart';

part 'country_entity.g.dart';

@JsonSerializable()
class CountryEntity {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'capital')
  String? capital;
  @JsonKey(name: 'region')
  String? region;
  @JsonKey(name: 'flag')
  String? flag;
  @JsonKey(name: 'population')
  int? population;

  CountryEntity({
    this.name,
    this.capital,
    this.region,
    this.flag,
    this.population,
  });

  factory CountryEntity.fromJson(Map<String, dynamic> json) =>
      _$CountryEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CountryEntityToJson(this);
}
