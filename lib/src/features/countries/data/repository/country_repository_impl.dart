import 'package:samim_test/src/core/models/api_response_to_result_model.dart';
import 'package:samim_test/src/core/models/api_result/result_model.dart';
import 'package:samim_test/src/features/countries/data/api/country_api/country_api.dart';
import 'package:samim_test/src/features/countries/data/mapper/country_entity_mapper.dart';
import 'package:samim_test/src/features/countries/domain/models/country_model/country_model.dart';
import 'package:samim_test/src/features/countries/domain/repository/country_repository/country_repository.dart';

class CountryRepositoryImpl extends CountryRepository {
  CountryApi countryApi;

  CountryRepositoryImpl({
    required this.countryApi,
  });

  @override
  Future<ResultModel<List<CountryModel>>> getCountries() async {
    final response = await countryApi.getCountries();

    return ApiToResultMapper.mapTo(
      response: response,
      dataMapper: () => List<CountryModel>.from(
          response.data!.map((countryEntity) => countryEntity.mapToModel())),
    );
  }
}
