import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:samim_test/src/core/models/api_response/api_response.dart';
import 'package:samim_test/src/features/countries/data/api/country_api/country_api.dart';
import 'package:samim_test/src/features/countries/data/entity/country_entity/country_entity.dart';
import 'package:samim_test/src/features/countries/data/repository/country_repository_impl.dart';
import 'package:samim_test/src/features/countries/domain/models/country_model/country_model.dart';
import 'package:samim_test/src/features/countries/domain/repository/country_repository/country_repository.dart';

import 'countries_repository.mocks.dart';

@GenerateMocks(<Type>[MockCountriesApi])
class MockCountriesApi extends Mock implements CountryApi {}

void main() {
  late MockMockCountriesApi mockCountriesApi;
  final List<CountryEntity> countryEntities = <CountryEntity>[
    CountryEntity(
      name: "Afghanistan",
      capital: "kabol",
      population: 10000,
      region: "asia",
    ),
  ];
  final List<CountryModel> countryModels = <CountryModel>[
    CountryModel(
      name: "Afghanistan",
      capital: "kabol",
      population: 10000,
      region: "asia",
    ),
  ];

  setUp(() {
    mockCountriesApi = MockMockCountriesApi();
  });

  test('getCountries should fetch countries', () async {
    /*-------------- Arrange -------------*/
    when(mockCountriesApi.getCountries()).thenAnswer(
        (_) async => ApiResponse(data: countryEntities, statusCode: 200));
    final CountryRepository countryRepository =
        CountryRepositoryImpl(countryApi: mockCountriesApi);

    /*--------------- Act ---------------*/
    final response = await countryRepository.getCountries();

    /*-------------- Assert -------------*/
    expect(countryModels, response.data);
  });

  test('getCountries should fetch countries with correct fields', () async {
    /*-------------- Arrange -------------*/
    when(mockCountriesApi.getCountries()).thenAnswer(
        (_) async => ApiResponse(data: countryEntities, statusCode: 200));
    final CountryRepository countryRepository =
        CountryRepositoryImpl(countryApi: mockCountriesApi);

    /*--------------- Act ---------------*/
    final response = await countryRepository.getCountries();

    /*-------------- Assert -------------*/
    expect(response.data?.first.name, "Afghanistan");
    expect(response.data?.first.capital, "kabol");
    expect(response.data?.first.population, 10000);
    expect(response.data?.first.region, "asia");
  });

  test('Country entity serialization/deserialization should work properly',
      () async {
    /*-------------- Arrange -------------*/
    final Map<String, dynamic> map = CountryEntity(
      name: "Afghanistan",
      capital: "kabol",
      population: 10000,
      region: "asia",
    ).toJson();

    /*--------------- Act ---------------*/
    final CountryEntity article = CountryEntity.fromJson(map);

    /*-------------- Assert -------------*/
    expect(article.name, "Afghanistan");
    expect(article.capital, "kabol");
    expect(article.population, 10000);
    expect(article.region, "asia");
  });
}
